FROM ubuntu

WORKDIR /synapse

RUN apt-get update && apt-get install --assume-yes build-essential python2.7-dev libffi-dev \
    python-pip python-setuptools sqlite3 git-core \
    libssl-dev python-virtualenv libjpeg-dev libxslt1-dev nano screen && \
    virtualenv -p python2.7 /synapse && \
    . /synapse/bin/activate && \
    pip install --upgrade pip && \
    pip install --upgrade setuptools && \
#    pip install git+https://gitlab.com/otau/synapse_server && \
    pip install https://github.com/matrix-org/synapse/tarball/master && \
    pip install psycopg2 && \
    pip install psycopg2-binary

EXPOSE 8008 8448 3478

CMD ["/synapse/bin/python","-B","-m","synapse.app.homeserver","--config-path","/synapse/data/homeserver.yaml"]
